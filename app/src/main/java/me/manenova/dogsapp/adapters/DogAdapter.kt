package me.manenova.dogsapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import me.manenova.dogsapp.R
import me.manenova.dogsapp.databinding.ItemDogBinding
import me.manenova.dogsapp.models.Dog

class DogAdapter (doglist: List<Dog>, private var listener:(Dog) -> Unit ) : RecyclerView.Adapter<DogAdapter.ViewHolder>() {

    private var doglist:List<Dog> = doglist

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v){
       // val  textDog: TextView = v.findViewById(R.id.text_dog)
        //val imageDog: ImageView = v.findViewById(R.id.imgDog)

        val binding = ItemDogBinding.bind(v)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v:View = LayoutInflater.from(parent.context).inflate(R.layout.item_dog,parent,false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dog = doglist[position]

        //holder.imageDog =
        holder.binding.textDog.text = dog.name

        holder.itemView.setOnClickListener{
            listener(dog)
        }

        Picasso.get().load(dog.imageURL).into(holder.binding.imgDog);
    }

    override fun getItemCount(): Int {
       return doglist.size
    }
}