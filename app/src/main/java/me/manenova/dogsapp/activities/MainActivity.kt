package me.manenova.dogsapp.activities

import android.content.Context
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils.replace
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.ListFragment
import androidx.fragment.app.commit
import androidx.navigation.fragment.NavHostFragment.findNavController

import me.manenova.dogsapp.databinding.ActivityMainBinding


var mContext: Context? = null
var mAppCompatActivity: AppCompatActivity? = null
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mContext = this;
        mAppCompatActivity = this

        setSupportActionBar(binding.toolbar)

    }

}