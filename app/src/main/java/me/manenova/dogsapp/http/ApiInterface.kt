package me.manenova.dogsapp.http

import me.manenova.dogsapp.models.MessageResponse
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET("breed/hound/images/random/30")
    fun getDogs(): Call<MessageResponse>
}